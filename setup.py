import setuptools
import os

with open('requirements.txt') as f:
    required = f.read().splitlines()

setuptools.setup(
    name="Email2Matrix",
    version="1.0a6",
    author="Adam S",
    author_email="adam@2isk.in",
    description="A simple application that relay email to Matrix.",
    url="https://gitlab.com/end_my_suffering/email2matrix",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires='>=3.8',
    install_requires=required,
)
