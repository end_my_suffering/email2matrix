import subprocess
import logging
import pathlib
import sys
import time
import utils
from uuid import uuid4


def send_to_matrix(matrix_queue, exit_event, config=None):
    logging.debug('send_to_matrix init')
    current_path = utils.current_path()
    uid = str(uuid4())

    if utils.is_bundled():
        logging.debug('running in a PyInstaller bundle')
        matrix_dir_path = current_path
        matrix_path = current_path / 'matrix-commander' / 'matrix-commander'
        matrix_exe = [matrix_path, "-p", uid, "-z"]

    else:
        logging.debug('running in a normal Python process')
        matrix_dir_path = current_path / 'matrix-commander'
        matrix_path = matrix_dir_path / 'matrix-commander.py'
        matrix_exe = [sys.executable, matrix_path, "-p", uid, "-z"]

    logging.debug(f"matrix_path: {matrix_path}")

    while True:
        if exit_event.is_set():
            break
        if not matrix_queue.empty():
            logging.debug('send_to_matrix received message')
            # Process msg
            msg_list = matrix_queue.get()
            msg_list.append('')
            msg = uid.join(msg_list)

            m_popen = subprocess.Popen(matrix_exe,
                                       cwd=matrix_dir_path,
                                       stdin=subprocess.PIPE,
                                       stdout=subprocess.PIPE,
                                       text=True
                                       )

            logging.debug(f"input={msg}")

            m_popen.communicate(input=msg)

        else:
            time.sleep(1)

    logging.debug('send_to_matrix exiting')
    return True
